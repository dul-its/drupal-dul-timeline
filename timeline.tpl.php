<?php
$thumbCount = 0;
$cbCount = 0;
$isCorp = false;
// test if it's a corporation
if (strpos($_SERVER['REQUEST_URI'], '/corporations') !== false ){
	$isCorp = true;
}

?>

<div class="timeline-wrapper">

<!-- Bio Text --->

	<div class="abstractBio">
		<div id="personPortrait">

			<?php echo '<a class="colorbox cboxElement" rel="colorbox" href="' . $dulmaster_headshot . '">'; ?>

				<?php echo '<img class="portrait subtleBorder smallShadow" property="schema:image" src="' . $dulmaster_headshot . '" alt="' . $dulmaster_name . '">'; ?>

			</a>
		</div>

		<h1 class="splashTitle" property="schema:name"><?php echo $dulmaster_name ?></h1>

		<div class="personAbout" property="schema:description">

			<?php echo $dulmaster_abstract ?>

		</div>

		<div class="clear"></div>


	</div>





<!-- Navigation Links -->

	<ul class="mainLinks">
		<li><a href="#timeline-embed" onclick="_gaq.push(['_trackEvent', 'eacNav','timelineLink']);">Timeline</a></li>

		<?php if ( isset($data_associatedcollections['values'][1]['0']) || isset($data_people['values'][1]['0']) || isset($data_corporatebodies['values'][1]['0']) ) { ?>

			<li><a href="#collections" onclick="_gaq.push(['_trackEvent', 'eacNav','collectionsLink']);">Collections</a></li>

		<?php } ?>

		<?php
		if ($isCorp == true) {
			echo '<li><a href="#history" onclick="_gaq.push([\'_trackEvent\', \'eacNav\',\'biogHistLink\']);">Administrative History</a></li>';
		}
		else {
			echo '<li><a href="#history" onclick="_gaq.push([\'_trackEvent\', \'eacNav\',\'biogHistLink\']);">Biographical History</a></li>';
		}
		?>


 	</ul>

 	<div class="clear"></div>





<!--- Timeline Embed --->


	<div class="iframe-timeline" id="timeline-embed">

		<?php // echo '<iframe src="//embed.verite.co/timeline/?source=' . $timeline_key . '&amp;font=Georgia-Helvetica&amp;maptype=toner&amp;lang=en&amp;height=450" width="100%" height="450" frameborder="0"></iframe>'; ?>

		<?php echo '<iframe src="https://cdn.knightlab.com/libs/timeline3/latest/embed/index.html?source=' . $timeline_key . '&amp;font=Georgia-Helvetica&amp;maptype=toner&amp;lang=en&amp;height=450" width="100%" height="450" frameborder="0"></iframe>'; ?>

	</div>


	<div style="text-align: center;">
		<p><a href="<?php echo 'https://cdn.knightlab.com/libs/timeline3/latest/embed/index.html?source=' . $timeline_key . '&amp;font=Georgia-Helvetica&amp;maptype=toner&amp;lang=en&amp;height=100%' ?>" target="_blank">View timeline in new window</a></p>
	</div>




	<!-- Entries: Column 1 -->

	<div class="grid-8 alpha">

		<?php

		if ($isCorp == true) {
			echo '<h2 id="history" class="bioTitle">Administrative History</h2>';
		}
		else {
			echo '<h2 id="history" class="bioTitle">Biographical History</h2>';
		}

		$loop_count = 0;

		foreach ($data_timeline['values'] as $item) {
			$loop_count++;
			if ($loop_count == 1) {
				continue;
			}

			// Start Date = ['0']
			// End Date = ['1']
			// Headline = ['2']
			// Text = ['3']
			// Media = ['4']
			// Media Credit = ['5']
			// Media Caption = ['6']
			// Media Thumbnail = ['7']


			// Normal entries
			if (isset($item['0'])) {

				$thumbCount = 0;

				$cbCount += 1;

				echo '<div class="item-wrapper">';

					echo '<div class="summary">';

						// Dates
						echo '<div class="dates">';

							// account for year only
							if (strlen($item['0']) == 4) {

								echo $item['0'];

							}

							else {

								echo date('Y M d', strtotime($item['0']));

							}

							// if there's an end date
							if (isset($item['1'])) {

								// account for year only
								if (strlen($item['0']) == 4) {

									echo ' &ndash; ' . $item['1'];

								}

								else {

									echo ' &ndash; ' . date('Y M d', strtotime($item['1']));

								}

							}

						echo '</div>';

						// Headline
						echo '<div class="headline">';

							echo $item['2'];

						echo '</div>';

					echo '</div>';

					echo '<div class="details">';


						// Thumbnail (if it's empty, display empty div)

						if (isset($item['7'])) {

							echo '<div class="timeline-thumbnail">';

								echo '<a class="colorbox cboxElement" data-group="colorboxgroup' . $cbCount . '" title="' . $item['6'] . '" href="' . $item['4'] . '">';

								echo '<img src="' . $item['7']. '" alt="' . $item['6'] . '" >';
								//echo '<span class="credit">' . $item['gsx$mediacredit']['$t'] . '</span>';

								echo '</a>';

							echo '</div>';

							}

							else {

								echo '<div class="timeline-thumbnail">&nbsp;</div>';

							}



						// Text Description
						echo '<div class="description">';
							echo '<p>' . $item['3'] . '</p>';
						echo '</div>';

					echo '</div>';

				echo '</div>';

				echo '<br clear="all" />';

			}

			// Extra images
			else {

				$thumbCount += 1;

				// Thumbnail
				echo '<div class="clipwrapper">';
					echo '<div class="clip">';

						echo '<a class="colorbox cboxElement" data-group="colorboxgroup' . $cbCount . '" title="' . $item['6'] . '" href="' . $item['4'] . '">';

						echo '<img src="' . $item['7'] . '" alt="' . $item['6'] . '" >';

						echo '</a>';

					echo '</div>';
				echo '</div>';

				if ($thumbCount % 4 == 0) {

					echo '<br clear="all" />';

				}

			}

		}

		unset($loop_count);

		?>

	</div>


	<!-- RELATIONS: Column 2 -->

	<div class="grid-4 omega" id="sidebar">

		<div id="accordion" style="max-height: 600px;">

			<!-- Collections Accordian -->

			<?php if(isset($data_associatedcollections['values'][1]['0'])) { ?>


			<h3 id="collections"><a href="#">Associated Collections</a></h3>
			<div>
				<ul id="collections_list">

				<?php

				// EAC URL = ['0']
            	// EAC Title = ['1']
            	// EAC Description = ['2']

				foreach ($data_associatedcollections['values'] as $item) {

					if ($item['0'] == 'EAC URL') {
						continue;
					}

					echo '<li>';

						echo '<a href="' . $item['0'] . '" class="linkFindingAid">' . $item['1'] . '</a><br>';

						if (isset($item['2'])) {
							echo '<span class="collectionDescription">' . $item['2'] . '</span>';
						}

					echo '</li>';

				}


				?>

				</ul>
    		</div>

    		<?php } ?>

    		<!-- People Accordian -->

    		<?php if(isset($data_people['values'][1]['0'])) { ?>

			<h3 id="people"><a href="#">People</a></h3>
			<div>

				<ul id="people_list">

				<?php

				// EAC URL = ['0']
            	// EAC Title = ['1']
            	// EAC Relation = ['2']

				foreach ($data_people['values'] as $item) {

					if ($item['0'] == 'EAC URL') {
						continue;
					}

					echo '<li>';

						echo '<a href="' . $item['0'] . '" class="linkFindingAid">' . $item['1'] . '</a> ' . $item['2'];

					echo '</li>';


				}


				?>


				</ul>
    		</div>

    		<?php } ?>


    		<!-- Corporate Bodies Accordian -->

    		<?php if (isset($data_corporatebodies['values'][1]['0'])) { ?>

			<h3 id="corporate_bodies"><a href="#">Corporate Bodies</a></h3>
			<div>
				<ul id="corporate_bodies_list">

				<?php

				// EAC URL = ['0']
            	// EAC Title = ['1']
            	// EAC Relation = ['2']

				foreach ($data_corporatebodies['values'] as $item) {
					
					if ($item['0'] == 'EAC URL') {
						continue;
					}

					echo '<li>';

						echo '<a href="' . $item['0'] . '" class="linkFindingAid">' . $item['1'] . '</a> ' . $item['2'];

					echo '</li>';

				}


				?>

				</ul>
    		</div>

    		<?php } ?>


		</div>

	</div>

</div>
