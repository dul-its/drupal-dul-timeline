<?php

//$myResult = db_query('SELECT * FROM {timeline} WHERE url_type = :url_type, array(':url_type' => $url_type''));

$result = db_query('SELECT url_slug, dulmaster_num, dulmaster_key FROM {timeline} n WHERE n.url_type = :url_type', array(':url_type' => $url_type));


// set timeout length
$ctx = stream_context_create(array(
	'http' => array(
		'timeout' => 5
		)
	)
);


?>

<div class="timeline-wrapper">

	<?php

	foreach ($result as $record) {

		$url_slug = $record->url_slug;
		$dulmaster_num = $record->dulmaster_num;
		$dulmaster_key = $record->dulmaster_key;

		if ( $url_slug == 'Slug' ) { // skip header row
			echo 'skipped';
			continue;
		}

		// Pull in Master Google Spreadsheet as JSON
		// $url_dulmaster = "https://spreadsheets.google.com/feeds/list/" . $dulmaster_key . "/od6/public/values?alt=json&amp;callback=displayContent";
		$url_dulmaster = "https://sheets.googleapis.com/v4/spreadsheets/" . $dulmaster_key . "/values/Sheet1?key=AIzaSyCRbmLIwwsgVKWq_0HftFij-6REppY1yFY";

		$json_dulmaster = file_get_contents($url_dulmaster, 0, $ctx);
		$data_dulmaster = json_decode($json_dulmaster, TRUE);

		$dulmaster_name = $data_dulmaster['values'][$dulmaster_num]['0'];
		$dulmaster_headshot = 'https://exhibits.library.duke.edu/uploads/digitalcollections/' . $data_dulmaster['values'][$dulmaster_num]['2'] . '/med/' . $data_dulmaster['values'][$dulmaster_num]['4'];
		$dulmaster_abstract = $data_dulmaster['values'][$dulmaster_num]['6'];

		$timeline_url = $GLOBALS['base_path'] . $root_path .'/' . $url_type . '/' . $url_slug;


		echo '<div class="list-item">';

		echo '<div class="thumbnail"><a href="' . $timeline_url . '"><img src="' . $dulmaster_headshot . '"></a></div>';

		echo '<h2><a href="' . $timeline_url . '">' . $dulmaster_name . '</a></h2>';

		echo $dulmaster_abstract;

		echo '</div>';


	}


	?>


</div>
